#!/bin/sh

if [ "$1" = "undo" ]; then
	for tera_file in ./templates/*.html.tera ./static/html/*.html; do
		for css_path in normalize styles big small; do
			sed -i -e "s/${css_path}_min.css/${css_path}.css/g" "$tera_file"
		done
	done
	rm -f ./static/css/*_min.css

	exit
fi

for css_file in ./static/css/{normalize.css,styles.css,big.css,small.css}; do
	yuicompressor --type css "$css_file" -o "./static/css/$(basename "$css_file" .css)_min.css"
done

for tera_file in ./templates/*.html.tera ./static/html/*.html; do
	for css_path in normalize styles big small; do
		sed -i -e "s/${css_path}.css/${css_path}_min.css/g" "$tera_file"
	done
done
