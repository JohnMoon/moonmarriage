use rocket_contrib::templates::Template; 

#[derive(Serialize)]
struct ErrorContext {
    code: u16,
    message: String
}

#[catch(400)]
pub fn bad_request() -> Template {
    let ctx = ErrorContext {
        code: 400,
        message: "Looks like that was a bad request - maybe try something different?".to_string()
    };

    Template::render("error", &ctx)
}

#[catch(404)]
pub fn not_found() -> Template {
    let ctx = ErrorContext {
        code: 404,
        message: "Hmmm, I don't have the page you're looking for!".to_string()
    };

    Template::render("error", &ctx)
}

#[catch(422)]
pub fn unproc_ent() -> Template {
    let ctx = ErrorContext {
        code: 422,
        message: "Hmm, something is wrong with that syntax!".to_string()
    };

    Template::render("error", &ctx)
}

#[catch(500)]
pub fn internal_error() -> Template {
    let ctx = ErrorContext {
        code: 500,
        message: "Whoops! Looks like something went wrong on my end...".to_string()
    };

    Template::render("error", &ctx)
}


