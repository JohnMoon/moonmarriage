use rocket_contrib::templates::Template;
use rocket::request::Form;
use std::fs::{File,OpenOptions};
use std::io::{BufReader,BufRead};
use std::io::prelude::*;

const GUEST_ID_MAP: &str = "./rsvp/guest_id_map.csv";
const GUEST_RSVPS: &str = "./rsvp/rsvps.tsv";

/// Template context passed to Tera template HTML file
#[derive(Serialize)]
pub struct RsvpContext {
    guest_list: Option<Vec<String>>,
    error: String
}

/// The IdForm sends the user's guess
#[derive(FromForm, Deserialize)]
pub struct IdForm {
    id: String,
}

#[derive(Debug, FromFormValue)]
enum Attending {
    Yes, No
}

#[derive(Debug, FromFormValue)]
enum Drink {
    LightBeer,
    DarkBeer,
    WhiteWine,
    RedWine,
    SoftDrinks
}

#[derive(Debug, FromForm)]
pub struct RsvpForm {
    name: String,
    attending: String,
    diet: String,
    drink: String,
    song: String
}

/// Route to deal with the downloader post data
#[get("/rsvp")]
pub fn rsvp() -> Template {
    let ctx = Some(RsvpContext {
        guest_list: None,
        error: "".to_string()
    });

    Template::render("rsvp", &ctx)
}

#[get("/RSVP")]
pub fn rsvp_caps() -> Template {
    rsvp()
}

/// Route to deal with the downloader post data
#[post("/rsvp", data = "<id_form>")]
pub fn rsvp_post(id_form: Form<IdForm>) -> Template {
    let mut error = "".to_string();
    let mut id = id_form.id.clone();
    let mut ctx: Option<RsvpContext> = {
        match generate_guest_table(&mut id) {
            Ok(c) => {
                Some(c)
            },
            Err(e) => {
                error = e;
                None
            }
        }
    };

    // In an error case, report the error to the user
    if ctx.is_none() {
        ctx = Some(RsvpContext {
            guest_list: None,
            error
        });
    }

    Template::render("rsvp", &ctx)
}

/// Route to deal with the downloader post data
#[post("/rsvp_confirm", data = "<rsvp_form>")]
pub fn rsvp_confirm(rsvp_form: Form<RsvpForm>) {
    println!("Got an RSVP: {:?}", rsvp_form);

    if let Ok(mut file) = OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open(GUEST_RSVPS) {

        if let Err(e) = writeln!(file, "{}\t{}\t{}\t{}\t{}",
                                 rsvp_form.name,
                                 rsvp_form.attending,
                                 rsvp_form.diet,
                                 rsvp_form.drink,
                                 rsvp_form.song) {
            println!("error writing to file: {}", e);
        }
    } else {
        println!("error opening file {}\n", GUEST_RSVPS);
    }

}

fn generate_guest_table(id: &mut String) -> Result<RsvpContext, String> {
    match get_guest_list(id) {
        Ok(g) => {
            Ok(RsvpContext {
                guest_list: Some(g),
                error: "".to_string()
            })
        },
        Err(e) => {
            Ok(RsvpContext {
                guest_list: None,
                error: e
            })
        }
    }
}

fn get_guest_list(id: &mut String) -> Result<Vec<String>, String> {
    if let Ok(gm_f) = File::open(GUEST_ID_MAP) {
        let gm = BufReader::new(&gm_f);
        let id_orig = id.clone();

        id.retain(|c| !c.is_whitespace());

        /* Each line is of format ID,First Last,First Last */
        for line in gm.lines() {
            let mut id_line: Vec<String> = line.unwrap().split(",").map(String::from).collect();
            let mut guests: Vec<_> = id_line.drain(1..).collect();
            guests.retain(|s| ! s.is_empty());

            let mut id_line_no_space = id_line[0].clone();
            id_line_no_space.retain(|c| !c.is_whitespace());

            if id_line_no_space.eq_ignore_ascii_case(&id) {
                return Ok(guests)
            }
        }

        Err(format!("Sorry, we couldn't find RSVP code \"{}\" in our database! Contact John or Hannah for help!", id_orig).to_string())
    } else {
        Err("Sorry, we couldn't find the guest to code mapping! Let John know ASAP!".to_string())
    }
}
