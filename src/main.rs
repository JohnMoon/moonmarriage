#![feature(proc_macro_hygiene, decl_macro, plugin)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
extern crate rocket_contrib;
extern crate serde_json;
extern crate time;
extern crate chrono;

use std::process::Command;
use std::path::{Path, PathBuf};
use rocket::request::Form;
use rocket::response::NamedFile;
use rocket::http::{Cookie, Cookies, SameSite};
use rocket_contrib::templates::Template;

mod catchers;
mod rsvp;

const H_EMAIL: &str = &include_str!("private/h_email.txt");
const H_PHONE: &str = &include_str!("private/h_phone.txt");
const J_EMAIL: &str = &include_str!("private/j_email.txt");
const J_PHONE: &str = &include_str!("private/j_phone.txt");
const BACKUP_QUOTE: &str = &include_str!("error/backup_quote.txt");
const TEST: &str = "all_attacks.txt";

/// Template context passed to Tera template HTML file
#[derive(Serialize)]
struct TemplateContext {
    quote: String,
    show_info: bool,
    user_guess: String,
    h_email: String,
    h_phone: String,
    j_email: String,
    j_phone: String
}

/// The QuoteForm sends the user's guess
#[derive(FromForm, Serialize)]
struct QuoteForm {
    user_guess: String
}

/// Generate a new quote using the "fortune" command (or a bacup quote if that fails)
fn get_new_quote() -> String {
    let mut ret = BACKUP_QUOTE.to_string();
    if let Ok(quote) = Command::new("fortune").arg("-a").arg("love_quotes").output() {
        if let Ok(quote) = String::from_utf8(quote.stdout) {
            ret = quote;
        } else {
            println!("error getting string from fortune stdout");
        }
    } else {
        println!("error running fortune");
    }

    ret
}

/// Route deals with the default GET path to the webroot
#[get("/")]
fn render_get(mut cookies: Cookies) -> Template {
    let quote = get_new_quote();
    cookies.add(Cookie::build("quote", quote.clone())
                .path("/")
                .http_only(true)
                .same_site(SameSite::Strict)
                .max_age(chrono::Duration::minutes(5))
                .finish());

    let ctx = TemplateContext {
        quote,
        show_info: false,
        user_guess: String::new(),
        h_email: String::new(),
        h_phone: String::new(),
        j_email: String::new(),
        j_phone: String::new()
    };

    Template::render("index", &ctx)
}

/// The POST route checks the user's guess and displays contact info if guess is correct
#[post("/", data = "<form>")]
fn render_post(cookies: Cookies, form: Form<QuoteForm>) -> Template {
    let mut quote = match cookies.get("quote") {
        Some(c) => c.to_string(),
        None => "".to_string()
    };

    // If there's no quote payload, just render the standard GET template
    if quote.is_empty() {
        render_get(cookies)
    } else {
        println!("pre-split: {}", quote);
        if let Some(q) = quote.split("=").nth(1) {
            quote = q.to_string();
        } else {
            quote = "".to_string();
        }
        println!("post-split: {}", quote);

        // If there is no third word in the quote, just say the user's guess is incorrect
        let third = match quote.split_whitespace().nth(2) {
            Some(s) => s.to_string(),
            None => "".to_string()
        };

        let show_info = {
            if third.is_empty() {
                quote = BACKUP_QUOTE.to_string();
                false
            } else {
                form.user_guess.eq_ignore_ascii_case(&third)
            }
        };

        let ctx = TemplateContext {
            quote,
            show_info,
            user_guess: form.user_guess.clone(),
            h_email: H_EMAIL.to_string(),
            h_phone: H_PHONE.to_string(),
            j_email: J_EMAIL.to_string(),
            j_phone: J_PHONE.to_string()
        };

        Template::render("index", &ctx)
    }
}

/// Route to load static files
#[get("/<file..>")]
fn static_files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

#[get("/our-story")]
fn our_story() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/html/our-story.html")).ok()
}

#[get("/travel")]
fn travel() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/html/travel.html")).ok()
}

#[get("/registry")]
fn registry() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/html/registry.html")).ok()
}

#[get("/rsvp_confirm")]
fn rsvp_confirm() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/html/rsvp_confirm.html")).ok()
}

#[get("/gallery")]
fn gallery() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/html/gallery.html")).ok()
}

/// Favicon route
#[get("/favicon.ico")]
fn favicon() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/images/favicon.ico")).ok()
}

#[get("/robots.txt")]
fn robots() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/files/robots.txt")).ok()
}

/// Main rocket ignition
fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .mount("/", routes![render_get,
                            render_post,
                            static_files,
                            our_story,
                            travel,
                            registry,
                            rsvp::rsvp,
                            rsvp::rsvp_caps,
                            rsvp::rsvp_post,
                            rsvp::rsvp_confirm,
                            rsvp_confirm,
                            gallery,
                            favicon,
                            robots])
        .register(catchers![catchers::internal_error,
                            catchers::unproc_ent,
                            catchers::bad_request,
                            catchers::not_found])
        .attach(Template::fairing())
}

fn main() {
    rocket().launch();
}

#[test]
fn test_quote_input() {
    use std::io::BufRead;

    let client = rocket::local::Client::new(rocket()).expect("valid rocket instance");

    // Pass malformed data and get a bad request
    let response = client.post("/")
        .body("user_guess=asdf&bad_field=bad_data")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::UnprocessableEntity);

    // Check that a reasonable guess returns an OK status
    let response = client.post("/")
        .body("user_guess=reasonable_guess")
        .header(rocket::http::ContentType::Form)
        .dispatch();
    assert_eq!(response.status(), rocket::http::Status::Ok);

    // Throw attack strings at the form
    let input_f = std::fs::File::open(TEST)
        .expect("error - input not found");
    let input = std::io::BufReader::new(&input_f);

    for attack in input.lines().map(Result::unwrap) {
        // All of these attacks should return a 200, 400, or 422 status
        let response = client.post("/")
            .body(format!("user_guess={}", attack))
            .header(rocket::http::ContentType::Form)
            .dispatch();

        assert!(response.status() == rocket::http::Status::Ok ||
                response.status() == rocket::http::Status::BadRequest ||
                response.status() == rocket::http::Status::UnprocessableEntity);
    }
}
