#!/bin/sh
if [ ! -d "$1" ]; then
	printf "Usage: %s <path to deployment directory>\n" "$0"
	exit 1
fi

deploy="$1"

cargo build --release
./minify_css.sh

sudo systemctl stop moonmarriage
sudo cp -ar ./target/release/moonmarriage ./static ./templates "$deploy"
sudo systemctl start moonmarriage
rm -f "${deploy}/static/css/"{normalize.css,styles.css,big.css,small.css}
